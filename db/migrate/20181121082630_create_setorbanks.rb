class CreateSetorbanks < ActiveRecord::Migration[5.2]
  def change
    create_table :setorbanks do |t|
      t.string :nama
      t.string :nominal
      t.string :keterangan

      t.timestamps
    end
  end
end
