class SetorbanksController < ApplicationController
  before_action :set_setorbank, only: [:show, :update, :destroy]

  # GET /setorbanks
  def index
    @setorbanks = Setorbank.all

    render json: @setorbanks
  end


  # POST /setorbanks
  def create
    ActiveRecord::Base.transaction do
    @setorbank = Setorbank.new(setorbank_params)
    firebase_url    = 'https://setorbank-3ee9d.firebaseio.com/'
    firebase_secret = '2kE4qkiOY3XgfJIObeY30CSdqz47VzSMLE0Oxdji'
    firebase = Firebase::Client.new(firebase_url, firebase_secret)
    @setorbank.lock!
    if @setorbank.save
      response = firebase.push("Backup Setoran", { :nama => @setorbank.nama,:nominal => @setorbank.nominal,:keterangan => @setorbank.keterangan, :waktuCreate => @setorbank.created_at, :waktuUpdate => @setorbank.updated_at })
      render json: @setorbank, status: :created
    else
      render json: @setorbank.errors, status: :unprocessable_entity
    end
  end
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setorbank
      @setorbank = Setorbank.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def setorbank_params
      params.require(:setorbank).permit(:nama, :nominal, :keterangan)
    end
end
